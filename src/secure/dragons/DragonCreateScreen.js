import React, { useContext, useState } from 'react';
import { Context as DragonContext } from '../../context/DragonContext';
import { useHistory } from 'react-router-dom';
import Wrapper from '../../components/common/Wrapper';
import Card from '../../components/common/Card';
import './DragonCreateScreen.css';

const DragonCreate = () => {
  let history = useHistory();

  const { state, createDragons } = useContext(DragonContext);
  const [name, setName] = useState('')
  const [type, setType] = useState('')

  const handleSubmit = async (e) => {
    e.preventDefault();
    const createdAt = new Date()
    await createDragons(name, type, createdAt)
    history.push('/')
  }

  return <Wrapper title="Create Dragon">
    <Card>
      <form onSubmit={handleSubmit}>
        <p>{state.errorMessage}</p>
        <label className="dragon-create-label">Nome:</label>
        <input className="dragon-create-input" type='name' name='name' value={name} onChange={(e) => setName(e.target.value)} required />
        <label className="dragon-create-label">Tipo:</label>
        <input className="dragon-create-input" type='type' name='type' value={type} onChange={(e) => setType(e.target.value)} required />
        <input className="dragon-create-button w3-hover-purple" type='submit' value="Cadastrar" />
      </form>
    </Card >
  </Wrapper>
}

export default DragonCreate

