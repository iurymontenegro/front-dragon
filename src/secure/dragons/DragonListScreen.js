import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Context as DragonContext } from '../../context/DragonContext';
import Wrapper from '../../components/common/Wrapper';
import DragonList from '../../components/DragonList';
import './DragonListScreen.css'

const DragonListScreen = () => {
  let history = useHistory();
  const { state, fetchDragons, editDragon, deleteDragon } = useContext(DragonContext);

  useEffect(() => {
    fetchDragons()
  }, []);

  const redirectToDragonCreate = (id) => {
    history.push(`/dragons/create`)
  }

  return <Wrapper title="Dragon List">
    <div className="w3-center">
      <button className="w3-button w3-circle dragon-add-button" onClick={redirectToDragonCreate}>+</button>
    </div>
    <DragonList dragons={state} deleteFunction={deleteDragon} updateFunction={editDragon} />
  </Wrapper>
}

export default DragonListScreen