import React, { useContext, useEffect, useState } from 'react';
import Card from '../../components/common/Card';
import Wrapper from '../../components/common/Wrapper';
import { Context as DragonContext } from '../../context/DragonContext';

const DragonDetail = (props) => {
  const { detailDragon } = useContext(DragonContext);
  const [dragon, setDragon] = useState({})

  useEffect(() => {
    async function fetchData() {
      const dragon = await detailDragon(props.match.params.id);
      setDragon(dragon);
    }
    fetchData();
  }, [detailDragon, props.match.params.id]);

  return (
    <Wrapper title="Dragon Details">
      {dragon && <Card>
        <h3 className="w3-center">Nome: {dragon.name}</h3>
        <h4 className="w3-center">Tipo: {dragon.type}</h4>
        <p className="w3-center">Data de Criação:</p>
        <p className="w3-center">{new Date(dragon.createdAt).toLocaleString('pt-BR')}</p>

        <p></p>
      </Card>}
    </Wrapper>
  )
}

export default DragonDetail