import React, { useEffect, useContext } from 'react';
import { Context as AuthContenxt } from '../context/AuthContext';
import { useHistory } from 'react-router-dom';

const ResolseAuthScreen = () => {
    const { state } = useContext(AuthContenxt);
    let history = useHistory();

    useEffect(() => {
        if (state.token) {
            history.push('/dragons')
        } else {
            history.push('/login')
        }
    }, [state.token]);

    return null
}

export default ResolseAuthScreen;