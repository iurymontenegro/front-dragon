import { BrowserRouter, Route } from 'react-router-dom';
import Login from './public/Login'
import ResolveAuth from './secure/ResolveAuthScreen'
import DragonListScreen from './secure/dragons/DragonListScreen';
import DragonDetailScreen from './secure/dragons/DragonDetailScreen';
import DragonCreateScreen from './secure/dragons/DragonCreateScreen';
import { Provider as AuthProvider } from './context/AuthContext';
import { Provider as DragonProvider } from './context/DragonContext';

function App() {
  return (
    <AuthProvider>
      <DragonProvider>
        <div>
          <BrowserRouter>
            <Route path={'/'} exact component={ResolveAuth} />
            <Route path={'/login'} component={Login} />
            <Route path={'/dragons'} exact component={DragonListScreen} />
            <Route path={'/dragon/:id'} component={DragonDetailScreen} />
            <Route path={'/dragons/create'} exact component={DragonCreateScreen} />
          </BrowserRouter>
        </div>
      </DragonProvider>
    </AuthProvider>
  );
}

export default App;
