// import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';


const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error':
      return { ...state, errorMessage: action.payload };
    case 'signin':
      return { errorMessage: '', token: action.payload };
    case 'clear_error_message':
      return { ...state, errorMessage: '' };
    case 'signout':
      return { token: null, errorMessage: '' }
    default:
      return state;
  }
};

const clearErrorMessage = dispatch => () => {
  dispatch({ type: 'clear_error_message' });
};

const signin = dispatch => async ({ email, password }) => {
  if (email === 'teste@teste.com' && password === '123456') {
    localStorage.setItem('authtoken', 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjUyMTJjM2Y5ZWVjZjIyYzEwODZiMDJmMDIzMDYwNjM4OGUzYmQ0ZGJiZTJmYWJhY2YwNWE5YTk5YzVlZThhYWMifQ.eyJhdWQiOlsiNmZlNjBmZTU4NDlmN2RlZjhjZTE0ZmIxODI1NDFmZWZmNDYyMDU1ZGFiYjRmNThhZTljZWFjMzIyMTFiZjM5NiJdLCJlbWFpbCI6ImltQGNvb2xkZXguY29tIiwiZXhwIjoxNjExODc0Mzc1LCJpYXQiOjE2MTE3ODc5NzUsIm5iZiI6MTYxMTc4Nzk3NSwiaXNzIjoiaHR0cHM6Ly9tYWlzYW1vci5jbG91ZGZsYXJlYWNjZXNzLmNvbSIsInR5cGUiOiJhcHAiLCJpZGVudGl0eV9ub25jZSI6IjdMOERoblh4NkdoYUxDU1AiLCJzdWIiOiJhZTI0NWFkOS1hNzVhLTRhMWUtOGM3OS04OTQxYmMyZDYwMWMiLCJjb3VudHJ5IjoiQlIifQ.D_JszMtkgX_X4Hknj04P9kiphbYyeEnvemwz7G6zOe0VBCDWYzVXAnInYBpLI7MU37qV9lRSJw0z8aDKO0xqJZVudxw3BL00fVpo2Xw8mC_08NlqC1_828bYLxkqloUGqTrUtcQGuPEXLxe2GJ7rwWq8yGfnRrmEer6yAayRfLowclIGWd4Gtr4JjriwDWKrgdiArX6MBw16E-wKmkU8E6K4JZG3yprZiCpGzpQR2o1Su4ARYhSTkmP0qGxGI_qF1vD-mT6-4hh9-x0XphB40yzT_R5djSIYtICgO6s0TaqYITYtPkR0FtJ-c11C6zxHU8NEmOgZszYHtfh5lag6MA');
    dispatch({ type: 'signin', payload: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjUyMTJjM2Y5ZWVjZjIyYzEwODZiMDJmMDIzMDYwNjM4OGUzYmQ0ZGJiZTJmYWJhY2YwNWE5YTk5YzVlZThhYWMifQ.eyJhdWQiOlsiNmZlNjBmZTU4NDlmN2RlZjhjZTE0ZmIxODI1NDFmZWZmNDYyMDU1ZGFiYjRmNThhZTljZWFjMzIyMTFiZjM5NiJdLCJlbWFpbCI6ImltQGNvb2xkZXguY29tIiwiZXhwIjoxNjExODc0Mzc1LCJpYXQiOjE2MTE3ODc5NzUsIm5iZiI6MTYxMTc4Nzk3NSwiaXNzIjoiaHR0cHM6Ly9tYWlzYW1vci5jbG91ZGZsYXJlYWNjZXNzLmNvbSIsInR5cGUiOiJhcHAiLCJpZGVudGl0eV9ub25jZSI6IjdMOERoblh4NkdoYUxDU1AiLCJzdWIiOiJhZTI0NWFkOS1hNzVhLTRhMWUtOGM3OS04OTQxYmMyZDYwMWMiLCJjb3VudHJ5IjoiQlIifQ.D_JszMtkgX_X4Hknj04P9kiphbYyeEnvemwz7G6zOe0VBCDWYzVXAnInYBpLI7MU37qV9lRSJw0z8aDKO0xqJZVudxw3BL00fVpo2Xw8mC_08NlqC1_828bYLxkqloUGqTrUtcQGuPEXLxe2GJ7rwWq8yGfnRrmEer6yAayRfLowclIGWd4Gtr4JjriwDWKrgdiArX6MBw16E-wKmkU8E6K4JZG3yprZiCpGzpQR2o1Su4ARYhSTkmP0qGxGI_qF1vD-mT6-4hh9-x0XphB40yzT_R5djSIYtICgO6s0TaqYITYtPkR0FtJ-c11C6zxHU8NEmOgZszYHtfh5lag6MA' });
  } else {
    dispatch({
      type: 'add_error',
      payload: 'Email ou Senha incorretos'
    });
  }
};

const signout = dispatch => async () => {
  localStorage.removeItem('authtoken');
  dispatch({ type: 'signout' });
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signin, signout, clearErrorMessage },
  { token: null, errorMessage: '' }
);
