import createDataContext from './createDataContext';
import dragonApi from '../api/dragons';

const dragonReducer = (state, action) => {
  switch (action.type) {
    case 'fetch_dragons':
      return action.payload;
    case 'create_dragons':
      return [...state, action.payload];
    case 'delete_dragons':
      return state.filter(dragon => dragon.id !== action.payload)
    default:
      return state;
  }
};

const fetchDragons = dispatch => async () => {
  const response = await dragonApi.get('/dragon');
  dispatch({ type: 'fetch_dragons', payload: response.data });
};

const createDragons = dispatch => async (name, type, createdAt) => {
  const response = await dragonApi.post('/dragon', { name, type, createdAt });
  dispatch({ type: 'create_dragons', payload: response.data });
};

const editDragon = dispatch => async (id, name, type) => {
  await dragonApi.put(`/dragon/${id}`, { name, type });
};

const deleteDragon = dispatch => async (id) => {
  await dragonApi.delete(`/dragon/${id}`);
  dispatch({ type: 'delete_dragons', payload: id });
};

const detailDragon = dispatch => async (id) => {
  const response = await dragonApi.get(`/dragon/${id}`);
  return response.data
}

export const { Context, Provider } = createDataContext(
  dragonReducer,
  { fetchDragons, createDragons, editDragon, deleteDragon, detailDragon },
  []
);