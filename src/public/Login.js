import React, { useEffect, useState, useContext } from 'react';
import { Context as AuthContext } from '../context/AuthContext';
import { useHistory } from 'react-router-dom';
import './Login.css'
import Card from '../components/common/Card';

const Login = () => {
  let history = useHistory();
  const { state, signin, clearErrorMessage } = useContext(AuthContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    if (state.token) {
      history.push('/')
    }
  }, [history, state.token]);

  const handleSubmit = (e) => {
    e.preventDefault();
    clearErrorMessage()
    signin({ email, password })
  }

  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <h2 className="login-title">Login</h2>
        <p className="error-menssage">{state.errorMessage}</p>
        <label className="login-label">Email:</label>
        <input className="login-input" type='email' name='email' value={email} onChange={(e) => setEmail(e.target.value)} required />
        <label className="login-label">Senha:</label>
        <input className="login-input" type='password' name='password' value={password} onChange={(e) => setPassword(e.target.value)} required />
        <input className="login-button w3-hover-purple" type='submit' value="Entrar" />
      </form>
    </Card>
  )
}

export default Login