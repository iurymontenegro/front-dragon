import React from 'react';
import DragonItem from './DragonItem'
import './DragonList.css'

const DragonList = ({ dragons, deleteFunction, updateFunction }) => {
  return (
    <div className="dragon-list">
      {dragons.sort(function (a, b) {
        if (a.name < b.name) {
          return -1;
        }
        if (b.name < a.name) {
          return 1;
        }
        return 0;
      }).map(dragon => <DragonItem key={dragon.id} dragon={dragon} deleteFunction={deleteFunction} updateFunction={updateFunction} />)}
    </div>
  )
}

export default DragonList