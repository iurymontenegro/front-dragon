import React, { useContext } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Context as AuthContext } from '../../context/AuthContext'
import './Wrapper.css';

const Wrapper = ({ title, children }) => {
  const { state, signout } = useContext(AuthContext);

  if (!state.token) {
    return <Redirect to={'/login'} />;
  }

  return (
    <div>
      <div className="w3-row navdragon">
        <div className="w3-col s2 m1 l1">
          <Link to={'/'}>Início</Link>
        </div>
        <div className="w3-center w3-col s8 m10 l10">{title}</div>
        <div className="w3-right w3-col s2 m1 l1 logout-button" onClick={() => signout()}>Sair</div>
      </div>
      {children}
    </div>
  )
}

export default Wrapper