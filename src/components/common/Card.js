import React from 'react';
import './Card.css';

const Card = ({ children }) => {
  return (
    <div className="card-dragon">
      {children}
    </div>
  )
}

export default Card