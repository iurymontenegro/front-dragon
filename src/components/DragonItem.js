import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './DragonItem.css'

const DragonIten = ({ dragon, deleteFunction, updateFunction }) => {
  let history = useHistory();

  const [editAction, setEditAction] = useState(false)
  const [name, setName] = useState(dragon.name)
  const [type, setType] = useState(dragon.type)

  const changeForEditable = () => {
    setEditAction(true)
  }

  const redirectToDragonDetail = (id) => {
    history.push(`/dragon/${id}`)
  }

  const saveDragonEdit = async () => {
    await updateFunction(dragon.id, name, type)
    setEditAction(false)
  }

  const cancelEditable = () => {
    setEditAction(false)
  }

  if (editAction) {
    return (
      <div className="w3-row dragon-item-card">
        <div className="w3-col s12 m6 l6">
          <div className="w3-col s6 m6 l6 dragon-text">
            <input className="dragon-input" type="text" defaultValue={name} onChange={(e) => setName(e.target.value)} />
          </div>
          <div className="w3-col s6 m6 l6 dragon-text">
            <input className="dragon-input" type="text" defaultValue={type} onChange={(e) => setType(e.target.value)} />
          </div>
        </div>
        <div className="w3-col s12 m6 l6">
          <div className="w3-col s6 m6 l6 w3-center">
            <button className="dragon-button-card w3-center" onClick={() => saveDragonEdit()}>Salvar</button>
          </div>
          <div className="w3-col s6 m6 l6 w3-center">
            <button className="dragon-button-card w3-center" onClick={() => cancelEditable()}>Cancelar</button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="w3-row dragon-item-card">
      <div className="w3-col s12 m6 l6">
        <div className="w3-col s6 m6 l6 dragon-text">Nome: {name}</div>
        <div className="w3-col s6 m6 l6 dragon-text">Tipo: {type}</div>
      </div>
      <div className="w3-col s12 m6 l6">
        <div className="w3-col s4 m4 l4 w3-center">
          <button className="dragon-button-card" onClick={() => redirectToDragonDetail(dragon.id)}>Detalhes</button>
        </div>
        <div className="w3-col s4 m4 l4 w3-center">
          <button className="dragon-button-card w3-center" onClick={() => changeForEditable()}>Editar</button>
        </div>
        <div className="w3-col s4 m4 l4 w3-center">
          <button className="dragon-button-card w3-center" onClick={() => deleteFunction(dragon.id)}>Apagar</button>
        </div>
      </div>
    </div>
  )
}

export default DragonIten