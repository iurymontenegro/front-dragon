# Solução para o desafio proposto pela South System

Para o login usar:

email -> teste@teste.com

senha -> 123456

preview page: https://60130a9f1768fe47115925d0--inspiring-brattain-2f44d9.netlify.app

Preview sem acesso as rotas sem https

# Objetivo
# Criar uma aplicação que contenha:

## Página de login:

Única página disponível se não estiver logado;

Criar um usuário básico para acesso.

## Uma página de lista de dragões:

Os nomes devem estar em ordem alfabética;

A partir da lista, deverá ser possível remover e alterar as informações dos dragões.

Uma página com os detalhes de um dragão específico:

Os seguintes dados devem ser apresentados na página: - Data de criação; - Nome; - Tipo;

## Uma página para cadastro de dragões:

Regras:

Layout responsivo;

Utilizar React ou Angular 4+ para o desenvolvimento;

Usar um sistema de controle de versão para entregar o teste (Github, Bitbucket, ...)

O uso de libs é livre.

O que será avaliado:

Organização do código;

Componentização das páginas;

Interface organizada e amigável;

Uso adequado do css (ou alguma biblioteca).


API
http://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/dragon

Lista de dragões: GET .../api/v1/dragon

Detalhes de dragões: GET .../api/v1/dragon/:id

Criação de um dragão: POST .../api/v1/dragon

Edição de um dragão: PUT .../api/v1/dragon/:id

Deleção de um dragão: DELETE .../api/v1/dragon/:id

Bom teste!



# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!